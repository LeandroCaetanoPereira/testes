#Projeto para realização do teste Caixa Eletrônico

Disponível no link:
<http://dojopuzzles.com/problemas/exibe/caixa-eletronico/>

>##Desenvolva um programa que simule a entrega de notas quando um cliente efetuar um saque em um caixa eletrônico. 

>Os requisitos básicos são os seguintes:

>Entregar o menor número de notas;
>É possível sacar o valor solicitado com as notas disponíveis;
>Saldo do cliente infinito;
>Quantidade de notas infinito (pode-se colocar um valor finito de cédulas para aumentar a dificuldade do problema);
>Notas disponíveis de R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00
>###Exemplos:

>Valor do Saque: R$ 30,00 – Resultado Esperado: Entregar 1 nota de R$20,00 e 1 nota de R$ 10,00.
>Valor do Saque: R$ 80,00 – Resultado Esperado: Entregar 1 nota de R$50,00 1 nota de R$ 20,00 e 1 nota de R$ 10,00.


##Detalhamento do projeto
Além da simulação de entrega de notas foi implementada a simulação da reposição de notas por um Operador
visando uma maior dificuldade na execução do teste.

O projeto utiliza o gerenciador de pacotes __Maven__ e o Framework __JUnit__ para testes de assertividade.
Visando a exemplificação de Design Patterns foi utilizado __Interface__.

O projeto não conta com uma interface gráfica apenas com um menu em modo texto para teste das operações.
O Caixa Eletrônico é iniciado sem nenhuma nota disponível.
As operações contempladas são:
*Reposição de Notas 
*Saque - Para efetuar o saque é necessário efetuar o Login, senha: 1234
