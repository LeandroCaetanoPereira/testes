package br.com.zenvia.facade;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Random;

import br.com.zenvia.model.Nota;
import br.com.zenvia.model.NotasEnum;

public class NotaFacadeImp implements NotaFacade{
	
	@Override
	public ArrayList<Nota> criarNotas(int quantidadeNotasCem, int quantidadeNotasCinquenta,
			 int quantidadeNotasVinte, int quantidadeNotasDez){
		ArrayList<Nota> notas = new ArrayList<Nota>();
		for(int i = 0; i<quantidadeNotasCem; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.CEM));			
		}		
		for(int i = 0; i<quantidadeNotasCinquenta; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.CINQUENTA));			
		}
		for(int i = 0; i<quantidadeNotasVinte; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.VINTE));			
		}
		for(int i = 0; i<quantidadeNotasDez; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.DEZ));			
		}
		return notas;
	}
}
