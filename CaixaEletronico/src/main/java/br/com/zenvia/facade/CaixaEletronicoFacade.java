package br.com.zenvia.facade;

import java.util.ArrayList;
import java.util.List;

import br.com.zenvia.model.CaixaEletronico;
import br.com.zenvia.model.Conta;
import br.com.zenvia.model.Nota;
import br.com.zenvia.model.NotasEnum;
import br.com.zenvia.model.Transacao;

public interface CaixaEletronicoFacade {
	
	public abstract void reporNotas(CaixaEletronico caixaEletronico, List<Nota> notas);
	
	public abstract ArrayList<Nota> separarNotas(CaixaEletronico caixaEletronico, int quantidadeNotasCem, int quantidadeNotasCinquenta,
											 	 int quantidadeNotasVinte, int quantidadeNotasDez);
	
	public abstract String validarValor(double valor);
	
	public abstract int[] calcularNotas(CaixaEletronico caixaEletronico, double valor);
	
	public abstract int contarNotas(List<Nota> notas, NotasEnum tipo);
	
	public abstract Transacao sacar(CaixaEletronico caixa, Conta conta, Double valor);
	
}
