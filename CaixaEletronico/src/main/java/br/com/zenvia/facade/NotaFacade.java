package br.com.zenvia.facade;

import java.util.ArrayList;

import br.com.zenvia.model.Nota;

public interface NotaFacade {
	
	public abstract ArrayList<Nota> criarNotas(int quantidadeNotasCem, int quantidadeNotasCinquenta,
			 								   int quantidadeNotasVinte, int quantidadeNotasDez);
	
}
