package br.com.zenvia.facade;

import br.com.zenvia.model.Conta;

public interface ContaFacade {
	
	public abstract void logar(Conta conta, int senha);
	
}
