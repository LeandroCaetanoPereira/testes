package br.com.zenvia.facade;

import br.com.zenvia.model.Conta;

public class ContaFacadeImp implements ContaFacade{
	
	@Override
	public void logar(Conta conta, int senha) {		
		if (conta.getSenha() == senha) {
			conta.setLogado(true);
		}else {
			conta.setLogado(false);
		}
	}	
}
