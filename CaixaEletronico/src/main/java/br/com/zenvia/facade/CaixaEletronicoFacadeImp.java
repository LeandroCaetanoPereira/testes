package br.com.zenvia.facade;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import br.com.zenvia.model.CaixaEletronico;
import br.com.zenvia.model.Conta;
import br.com.zenvia.model.Nota;
import br.com.zenvia.model.NotasEnum;
import br.com.zenvia.model.Transacao;
import br.com.zenvia.model.TransacoesEnum;

public class CaixaEletronicoFacadeImp implements CaixaEletronicoFacade{
	
	public void reporNotas(CaixaEletronico caixaEletronico, List<Nota> notas) {
		if (notas != null){
			for(Nota nota : notas) {
				switch (nota.getValor()){
					case CEM:
						caixaEletronico.setQuantidadeNotasCem(caixaEletronico.getQuantidadeNotasCem()+1);
						break;
					case CINQUENTA:
						caixaEletronico.setQuantidadeNotasCinquenta(caixaEletronico.getQuantidadeNotasCinquenta()+1);
						break;
					case VINTE:
						caixaEletronico.setQuantidadeNotasVinte(caixaEletronico.getQuantidadeNotasVinte()+1);
						break;
					case DEZ:
						caixaEletronico.setQuantidadeNotasDez(caixaEletronico.getQuantidadeNotasDez()+1);
						break;
					default:
						break;
				}
			}
		}
	}
	
	public ArrayList<Nota> separarNotas(CaixaEletronico caixaEletronico, int quantidadeNotasCem, 
										int quantidadeNotasCinquenta, int quantidadeNotasVinte, 
										int quantidadeNotasDez){
		ArrayList<Nota> notas = new ArrayList<Nota>();
		for(int i = 0; i<quantidadeNotasCem; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.CEM));
			caixaEletronico.setQuantidadeNotasCem(caixaEletronico.getQuantidadeNotasCem()-1);
		}		
		for(int i = 0; i<quantidadeNotasCinquenta; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.CINQUENTA));
			caixaEletronico.setQuantidadeNotasCinquenta(caixaEletronico.getQuantidadeNotasCinquenta()-1);
		}
		for(int i = 0; i<quantidadeNotasVinte; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.VINTE));
			caixaEletronico.setQuantidadeNotasVinte(caixaEletronico.getQuantidadeNotasVinte()-1);
		}
		for(int i = 0; i<quantidadeNotasDez; i++) {
			byte[] array = new byte[12]; 
			new Random().nextBytes(array);			    
			notas.add(new Nota(new String(array, Charset.forName("UTF-8")), NotasEnum.DEZ));
			caixaEletronico.setQuantidadeNotasDez(caixaEletronico.getQuantidadeNotasDez()-1);
		}
		return notas;
	}
	
	public String validarValor(double valor) {
		String mensagem = null;
		if(valor <= 0) {
			mensagem = "Valor do Saque deve ser maior do que zero.";
			return mensagem;
		}
		if (valor % 10  != 0){		
			mensagem = "Não será possível realizar o saque deste valor. Notas disponíveis: 100, 50, 20, 10.";
			return mensagem;
		}
		return "Valor válido";
	}
	
	public int[] calcularNotas(CaixaEletronico caixaEletronico, double valor){
		int notas[] = new int[4];	
		double valorRestante = valor;
		notas[0] = calcularNotasCem(caixaEletronico, valorRestante);
		valorRestante = valorRestante - (notas[0] * 100);
		notas[1] = calcularNotasCinquenta(caixaEletronico, valorRestante);
		valorRestante = valorRestante - (notas[1] * 50);
		notas[2] = calcularNotasVinte(caixaEletronico, valorRestante);
		valorRestante = valorRestante - (notas[2] * 20);
		notas[3] = calcularNotasDez(caixaEletronico, valorRestante);
		valorRestante = valorRestante - (notas[3] * 10);
		return notas;
	}
	
	private int calcularNotasCem(CaixaEletronico caixaEletronico, double valor){
		Double quantidadeNotasCem = valor / 100;
		if (quantidadeNotasCem.intValue() <= caixaEletronico.getQuantidadeNotasCem()) {
			quantidadeNotasCem.intValue();
		}else {
			quantidadeNotasCem = new Double(caixaEletronico.getQuantidadeNotasCem());
		}
		return quantidadeNotasCem.intValue();
	}
	
	private int calcularNotasCinquenta(CaixaEletronico caixaEletronico, double valor){
		Double quantidadeNotasCinquenta = valor / 50;
		if (quantidadeNotasCinquenta.intValue() <= caixaEletronico.getQuantidadeNotasCinquenta()) {
			quantidadeNotasCinquenta.intValue();
		}else {
			quantidadeNotasCinquenta = new Double(caixaEletronico.getQuantidadeNotasCinquenta());
		}
		return quantidadeNotasCinquenta.intValue();
	}
	
	private int calcularNotasVinte(CaixaEletronico caixaEletronico, double valor){
		Double quantidadeNotasVinte = valor / 20;
		if (quantidadeNotasVinte.intValue() <= caixaEletronico.getQuantidadeNotasVinte()) {
			quantidadeNotasVinte.intValue();
		}else {
			quantidadeNotasVinte = new Double(caixaEletronico.getQuantidadeNotasVinte());
		}
		return quantidadeNotasVinte.intValue();
	}
	
	private int calcularNotasDez(CaixaEletronico caixaEletronico, double valor){
		Double quantidadeNotasDez = valor / 10;
		if (quantidadeNotasDez.intValue() <= caixaEletronico.getQuantidadeNotasDez()) {
			quantidadeNotasDez.intValue();
		}else {
			quantidadeNotasDez = new Double(caixaEletronico.getQuantidadeNotasDez());
		}		
		return quantidadeNotasDez.intValue();
	}
	
	public int contarNotas(List<Nota> notas, NotasEnum tipo) {
		return notas.stream()
			   .filter(nota -> nota.getValor().equals(tipo))
			   .collect(Collectors.toList()).size();
	}
	
	@Override
	public Transacao sacar(CaixaEletronico caixa, Conta conta, Double valor) {		
		Transacao transacao = new Transacao(caixa, new Date(), valor, TransacoesEnum.SAQUE);		
		List<Nota> notas = new ArrayList<Nota>();
		//Verifica se o usuário está logado
		if(conta.isLogado()) {			
			//Verifica se o valor é maior que 0 e se as notas possíveis atendem ao valor solicitado
			String valorValido = validarValor(valor);
			if(valorValido.equals("Valor válido")) {
				//Distribui as notas necessárias de acordo com o valor solicitado
				int quantidadeNotas[] = calcularNotas(caixa, valor);
				notas = separarNotas(caixa, quantidadeNotas[0], quantidadeNotas[1], quantidadeNotas[2], quantidadeNotas[3]);
				StringBuffer mensagem = new StringBuffer();
				if(notas.stream().mapToInt(n -> n.getValor().valor).sum() == valor.intValue()) {
					mensagem.append("Saque efetuado com sucesso. Retire seu dinheiro.\n");
					mensagem.append(contarNotas(notas, NotasEnum.CEM) + " nota(s) de Cem.\n");
					mensagem.append(contarNotas(notas, NotasEnum.CINQUENTA) + " nota(s) de Cinquenta.\n");
					mensagem.append(contarNotas(notas, NotasEnum.VINTE) + " nota(s) de Vinte.\n");
					mensagem.append(contarNotas(notas, NotasEnum.DEZ) + " nota(s) de Dez.");	
					transacao.setMensagemRetorno(mensagem.toString());					
				}else {
					mensagem.append("Não será possível realizar o saque deste valor.\n");
					reporNotas(caixa, notas);
					if(caixa.getQuantidadeNotasCem() + caixa.getQuantidadeNotasCinquenta() + caixa.getQuantidadeNotasVinte() + caixa.getQuantidadeNotasDez() == 0 || notas.stream().mapToInt(n -> n.getValor().valor).sum() < valor.intValue()) {
						mensagem.append("Não existem notas disponíveis");
					}else {
						String notasDisponiveis = "";
						if(caixa.getQuantidadeNotasCem() > 0)
							notasDisponiveis += "100";
						if(caixa.getQuantidadeNotasCinquenta() > 0)
							notasDisponiveis += (notasDisponiveis.isEmpty() ? "50" : ", 50");	
						if(caixa.getQuantidadeNotasVinte() > 0)
							notasDisponiveis += (notasDisponiveis.isEmpty() ? "20" : ", 20");
						if(caixa.getQuantidadeNotasDez() > 0)
							notasDisponiveis += (notasDisponiveis.isEmpty() ? "10" : ", 10");
						mensagem.append("Notas disponíveis: ");
						mensagem.append(notasDisponiveis);
					}					
					mensagem.append(".");
					transacao.setMensagemRetorno(mensagem.toString());
				}				
			}else {
				transacao.setMensagemRetorno(valorValido);
			}
		}else {
			transacao.setMensagemRetorno("Usuário deve estar logado para realização do Saque.");
		}
		transacao.setNotas(notas);
		return transacao;
	}
}
