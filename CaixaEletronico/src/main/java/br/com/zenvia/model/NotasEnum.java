package br.com.zenvia.model;

public enum NotasEnum {
	CEM(100), CINQUENTA(50), VINTE(20), DEZ(10);
	
	public int valor;
	
	NotasEnum(int valor){
		this.valor = valor;
	}
}
