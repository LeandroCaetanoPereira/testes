package br.com.zenvia.model;

public class Conta {
	
	private long numero;
	private int senha;
	private Pessoa pessoa;
	private boolean logado;
	
	public Conta(Pessoa pessoa, long numero, int senha){
		this.setPessoa(pessoa);
		this.setNumero(numero);
		this.setSenha(senha);		
	}
	
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public int getSenha() {
		return senha;
	}
	public void setSenha(Integer senha) {
		this.senha = senha;
	}
	
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public boolean isLogado() {
		return logado;
	}

	public void setLogado(boolean logado) {
		this.logado = logado;
	}
}
