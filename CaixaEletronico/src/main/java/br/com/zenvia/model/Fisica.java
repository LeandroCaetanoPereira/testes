package br.com.zenvia.model;

public class Fisica extends Pessoa{
	
	private long CPF;
	
	public Fisica(String nome, String endereco, String email,
				  boolean situacao, long cPF) {		
		this.setNome(nome);
		this.setEndereco(endereco);
		this.setEmail(email);
		this.setSituacao(situacao);
		this.setTipoPessoa(true);
		this.setCPF(cPF);
	}
		
	public long getCPF() {
		return CPF;
	}

	public void setCPF(long cPF) {
		CPF = cPF;
	}

}
