package br.com.zenvia.model;

import java.util.Date;
import java.util.List;

public class Transacao {
	
	private Date data;
	private Double valor;
	private TransacoesEnum tipo;
	private String mensagemRetorno; 
	private CaixaEletronico caixa;
	private List<Nota> notas;
	
	public Transacao(CaixaEletronico caixa, Date data, Double valor, TransacoesEnum tipo) {
		setCaixa(caixa);
		setData(data);
		setValor(valor);
		setTipo(tipo);
	}
	
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public TransacoesEnum getTipo() {
		return tipo;
	}
	public void setTipo(TransacoesEnum tipo) {
		this.tipo = tipo;
	}
	public String getMensagemRetorno() {
		return mensagemRetorno;
	}
	public void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}

	public CaixaEletronico getCaixa() {
		return caixa;
	}

	public void setCaixa(CaixaEletronico caixa) {
		this.caixa = caixa;
	}

	public List<Nota> getNotas() {
		return notas;
	}

	public void setNotas(List<Nota> notas) {
		this.notas = notas;
	}
}
