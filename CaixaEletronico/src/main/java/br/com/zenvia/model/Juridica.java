package br.com.zenvia.model;

public class Juridica extends Pessoa {
	
	private long CNPJ;
	
	public Juridica(String nome, String endereco, String email,
			  boolean situacao, long cNPJ) {		
		this.setNome(nome);
		this.setEndereco(endereco);
		this.setEmail(email);
		this.setSituacao(situacao);
		this.setTipoPessoa(false);
		this.setCNPJ(cNPJ);
	}
	
	public long getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(long cNPJ) {
		CNPJ = cNPJ;
	}
}
