package br.com.zenvia.model;

public class CaixaEletronico {
	
	private long numero;
	private int quantidadeNotasCem;
	private int quantidadeNotasCinquenta;
	private int quantidadeNotasVinte;
	private int quantidadeNotasDez;
	
	public CaixaEletronico(long numero, int quantidadeNotasCem, int quantidadeNotasCinquenta,
						   int quantidadeNotasVinte, int quantidadeNotasDez) {
		this.numero = numero;
		this.quantidadeNotasCem = quantidadeNotasCem;
		this.quantidadeNotasCinquenta = quantidadeNotasCinquenta;
		this.quantidadeNotasVinte = quantidadeNotasVinte;
		this.quantidadeNotasDez = quantidadeNotasDez;		
	}
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public int getQuantidadeNotasCem() {
		return quantidadeNotasCem;
	}
	public void setQuantidadeNotasCem(int quantidadeNotasCem) {
		this.quantidadeNotasCem = quantidadeNotasCem;
	}
	public int getQuantidadeNotasCinquenta() {
		return quantidadeNotasCinquenta;
	}
	public void setQuantidadeNotasCinquenta(int quantidadeNotasCinquenta) {
		this.quantidadeNotasCinquenta = quantidadeNotasCinquenta;
	}
	public int getQuantidadeNotasVinte() {
		return quantidadeNotasVinte;
	}
	public void setQuantidadeNotasVinte(int quantidadeNotasVinte) {
		this.quantidadeNotasVinte = quantidadeNotasVinte;
	}
	public int getQuantidadeNotasDez() {
		return quantidadeNotasDez;
	}
	public void setQuantidadeNotasDez(int quantidadeNotasDez) {
		this.quantidadeNotasDez = quantidadeNotasDez;
	}
}
