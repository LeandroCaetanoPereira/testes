package br.com.zenvia.model;

public enum TransacoesEnum {
	SAQUE(1), DEPOSITO(2), TRANSFERENCIA(3);
	
	public int transacao;
	
	TransacoesEnum(int transacao){
		this.transacao = transacao;
	}
	
}
