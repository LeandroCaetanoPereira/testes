package br.com.zenvia.model;

public class Nota {
	
	private String serie;
	private NotasEnum valor;
	
	public Nota(String serie, NotasEnum valor) {
		setSerie(serie);
		setValor(valor);
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public NotasEnum getValor() {
		return valor;
	}
	public void setValor(NotasEnum valor) {
		this.valor = valor;
	}
}
