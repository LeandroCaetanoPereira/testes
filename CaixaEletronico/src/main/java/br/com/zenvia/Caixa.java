package br.com.zenvia;

import java.util.InputMismatchException;
import java.util.Scanner;

import br.com.zenvia.facade.CaixaEletronicoFacadeImp;
import br.com.zenvia.facade.ContaFacadeImp;
import br.com.zenvia.facade.NotaFacadeImp;
import br.com.zenvia.model.CaixaEletronico;
import br.com.zenvia.model.Conta;
import br.com.zenvia.model.Fisica;
import br.com.zenvia.model.Transacao;

public class Caixa {
	
	static ContaFacadeImp contaFacade = new ContaFacadeImp();
	static NotaFacadeImp notaFacade = new NotaFacadeImp();
	static CaixaEletronicoFacadeImp caixaFacade = new CaixaEletronicoFacadeImp();
	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);		
		int menu = -1;
		CaixaEletronico caixa = new CaixaEletronico(1L, 0, 0, 0, 0);
		menuInicial();
		while (menu != 0) {			
			try {
				menu = reader.nextInt();
				switch (menu) {
					case 0:
						System.out.println("Operação Finalizada");
						System.exit(0);
					case 1:
						menuOperador(reader, caixa);
						break;
					case 2:
						Fisica pessoa = new Fisica("Leandro", "Rua das Perdizes", "leocpnet@gmail.com", true, 99999999999L);
						Conta conta = new Conta(pessoa, 123456789L, 1234);
						menuCliente(reader, caixa, conta);
						break;
					default:
						System.err.println("Menu Inválido");
						menu = -1;
						break;
				}
			}catch(InputMismatchException e) {
				System.err.println("Menu Inválido");
				menu = -1;
			}
		}
		reader.close();
		System.out.println("Operação Finalizada");
		System.exit(0);
		
	}
	
	static void menuInicial() {		
		System.out.println("Selecione o Menu desejado: " + "\n 1 - Operador                2 - Cliente                0 - Sair" );
				
	}
	
	static void menuOperador(Scanner reader, CaixaEletronico caixa) {		
		System.out.println("Selecione a Operação desejada: " + "\n 1 - Reposição de Notas                2 - Voltar                0 - Sair");
		int menu = -1;
		while (menu != 0) {			
			try {
				menu = reader.nextInt();
				switch (menu) {
					case 0:
						System.out.println("Operação Finalizada");
						System.exit(0);
					case 1:
						menuNotas(reader, caixa);
						menu = 0;
						menuOperador(reader, caixa);
						break;
					case 2:		
						menuInicial();
						menu = 0;
						break;						
					default:
						System.err.println("Menu Inválido");
						menu = -1;
						break;
				}
			}catch(InputMismatchException e) {
				System.err.println("Menu Inválido");
				menu = -1;
				reader.next();
			}
		}
	}
	
	static void menuCliente(Scanner reader, CaixaEletronico caixa, Conta conta) {
		if(!conta.isLogado()) {
			System.out.println("Seja bem vindo " + conta.getPessoa().getNome() + ". \nDigite a senha:");
		}
		int tentativas = 3;
		while (tentativas > 0) {
			if(!conta.isLogado()) {
				int senha = reader.nextInt();
				contaFacade.logar(conta, senha);
				if(conta.isLogado()){
					System.out.println("Seja bem vindo a sua Conta " + conta.getNumero() + ". \n");
				}
			}
			if(conta.isLogado()) {					
				tentativas = 0;
				System.out.println("Selecione a Operação desejada: " + "\n 1 - Sacar                2 - Voltar                0 - Sair");
				int menu = -1;
				while (menu != 0) {			
					try {
						menu = reader.nextInt();
						switch (menu) {
							case 0:
								System.out.println("Operação Finalizada");
								System.exit(0);
							case 1:
								menuSacar(reader, caixa, conta);
								menu = 0;
								menuCliente(reader, caixa, conta);
								break;						
							case 2:
								menuInicial();
								menu = 0;
								break;
							default:
								System.err.println("Menu Inválido");
								menu = -1;
								break;
						}
					}catch(InputMismatchException e) {
						System.err.println("Menu Inválido");
						reader.next();
					}
				}
			}else {
				tentativas--;
				System.out.println("Senha inválida, tente novamente, restam " + tentativas + " tentativas.");
			}
		}
	}
	
	static void menuNotas(Scanner reader, CaixaEletronico caixa) {				
		int reposicaoConcluida = 0;
		int quantidadeNotasCem = 0;
		int quantidadeNotasCinquenta = 0;
		int quantidadeNotasVinte = 0;
		int quantidadeNotasDez = 0;
		while(reposicaoConcluida < 4){
			try {
				if(reposicaoConcluida == 0) {
					System.out.println("Informe a quantidade de Notas de 100:");
					quantidadeNotasCem = reader.nextInt();
					reposicaoConcluida = 1;
				}
				if(reposicaoConcluida == 1) {
					System.out.println("Informe a quantidade de Notas de 50:");
					quantidadeNotasCinquenta = reader.nextInt();
					reposicaoConcluida = 2;
				}
				if(reposicaoConcluida == 2) {
					System.out.println("Informe a quantidade de Notas de 20:");
					quantidadeNotasVinte = reader.nextInt();
					reposicaoConcluida = 3;
				}
				if(reposicaoConcluida == 3) {
					System.out.println("Informe a quantidade de Notas de 10:");
					quantidadeNotasDez = reader.nextInt();
					reposicaoConcluida = 4;
				}
				caixaFacade.reporNotas(caixa, notaFacade.criarNotas(quantidadeNotasCem, quantidadeNotasCinquenta, quantidadeNotasVinte, quantidadeNotasDez));
				System.out.println("Reposição de Notas concluída.");			
			}catch(InputMismatchException e) {
				System.err.println("Deve ser informado valor inteiro para Notas.");
				reader.next();
			}
		}
	}
	
	static void menuSacar(Scanner reader, CaixaEletronico caixa, Conta conta) {
		Transacao transacao = null;
		while(transacao == null) {
			try {
				System.out.println("Digite o valor desejado:");
				double valorSaque = reader.nextInt();
				transacao = caixaFacade.sacar(caixa, conta, valorSaque);
				System.out.println(transacao.getMensagemRetorno());			
			}catch(InputMismatchException e) {
				System.err.println("Deve ser informado valor inteiro.");
				reader.next();
			}
		}		
	}
}
