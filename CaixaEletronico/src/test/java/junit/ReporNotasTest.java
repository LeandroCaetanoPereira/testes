package junit;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import br.com.zenvia.facade.CaixaEletronicoFacadeImp;
import br.com.zenvia.facade.NotaFacadeImp;
import br.com.zenvia.model.CaixaEletronico;
import br.com.zenvia.model.Nota;

public class ReporNotasTest {
	
	CaixaEletronicoFacadeImp caixaFacade = new CaixaEletronicoFacadeImp();
	NotaFacadeImp notaFacade = new NotaFacadeImp();
	
	@Test
	public void test() {
		
		CaixaEletronico caixaAtual = new CaixaEletronico(1L, 0, 0, 0, 0);
		
		//Notas Nulo
		caixaFacade.reporNotas(caixaAtual, null);
		
		//Situa��o do Caixa Esperada ap�s a atualiza��o
		assertEquals(0, caixaAtual.getQuantidadeNotasCem());
		assertEquals(0, caixaAtual.getQuantidadeNotasCinquenta());
		assertEquals(0, caixaAtual.getQuantidadeNotasVinte());
		assertEquals(0, caixaAtual.getQuantidadeNotasDez());
				
		//Array vazio
		caixaFacade.reporNotas(caixaAtual, new ArrayList<Nota>());
		
		//Situa��o do Caixa Esperada ap�s a atualiza��o
		assertEquals(0, caixaAtual.getQuantidadeNotasCem());
		assertEquals(0, caixaAtual.getQuantidadeNotasCinquenta());
		assertEquals(0, caixaAtual.getQuantidadeNotasVinte());
		assertEquals(0, caixaAtual.getQuantidadeNotasDez());
		
		//Array com valores v�lidos
		caixaFacade.reporNotas(caixaAtual, notaFacade.criarNotas(100, 200, 500, 1000));
		
		CaixaEletronico caixaEsperado =  new CaixaEletronico(1L, 0, 0, 0, 0);
		caixaEsperado.setNumero(1L);
		caixaEsperado.setQuantidadeNotasCem(100);
		caixaEsperado.setQuantidadeNotasCinquenta(200);
		caixaEsperado.setQuantidadeNotasVinte(500);
		caixaEsperado.setQuantidadeNotasDez(1000);
		
		
		//Situa��o do Caixa Esperada ap�s a atualiza��o
		assertEquals(caixaEsperado.getQuantidadeNotasCem(), caixaAtual.getQuantidadeNotasCem());
		assertEquals(caixaEsperado.getQuantidadeNotasCinquenta(), caixaAtual.getQuantidadeNotasCinquenta());
		assertEquals(caixaEsperado.getQuantidadeNotasVinte(), caixaAtual.getQuantidadeNotasVinte());
		assertEquals(caixaEsperado.getQuantidadeNotasDez(), caixaAtual.getQuantidadeNotasDez());
	}
}
