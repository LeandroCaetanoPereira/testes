package junit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.zenvia.facade.CaixaEletronicoFacadeImp;
import br.com.zenvia.facade.ContaFacadeImp;
import br.com.zenvia.model.CaixaEletronico;
import br.com.zenvia.model.Conta;
import br.com.zenvia.model.Fisica;
import br.com.zenvia.model.NotasEnum;
import br.com.zenvia.model.Transacao;

public class SacarTest {

	CaixaEletronicoFacadeImp caixaFacade = new CaixaEletronicoFacadeImp();
	ContaFacadeImp contaFacade = new ContaFacadeImp();
	
	@Test
	public void test() {
		//Caixa Eletrônico sem notas disponíveis
		CaixaEletronico caixa = new CaixaEletronico(1L, 0, 0, 0, 0);
		Fisica pessoa = new Fisica("Leandro", "Rua das Perdizes", "leocpnet@gmail.com", true, 99999999999L);
		Conta conta = new Conta(pessoa, 123456789L, 1234);

		//Tentativa de saque antes de login;
		assertEquals("Usuário deve estar logado para realização do Saque.", caixaFacade.sacar(caixa, conta, new Double(100)).getMensagemRetorno());
		
		contaFacade.logar(conta, 1234);

		//Tentativa de saque com valor inválido;
		assertEquals("Valor do Saque deve ser maior do que zero.", caixaFacade.sacar(caixa, conta, new Double(0)).getMensagemRetorno());
		
		//Tentativa de saque com valor inválido;
		assertEquals("Não será possível realizar o saque deste valor. Notas disponíveis: 100, 50, 20, 10.", caixaFacade.sacar(caixa, conta, new Double(15)).getMensagemRetorno());
		
		//Caixa Eletrônico com 1000 notas de vinte disponíveis 
		caixa = new CaixaEletronico(1L, 0, 0, 1000, 0);
				
		//Validação de Notas Liberadas
		Transacao transacao = caixaFacade.sacar(caixa, conta, new Double(550));
		assertEquals("Não será possível realizar o saque deste valor.\nNão existem notas disponíveis.", transacao.getMensagemRetorno());
		
		//Saque efetuado com sucesso;
		transacao = caixaFacade.sacar(caixa, conta, new Double(560));
		assertEquals("Saque efetuado com sucesso. Retire seu dinheiro.\n" +
					 "0 nota(s) de Cem.\n" + 
					 "0 nota(s) de Cinquenta.\n" + 
					 "28 nota(s) de Vinte.\n" + 
					 "0 nota(s) de Dez.", transacao.getMensagemRetorno());
		

		assertEquals(0, caixaFacade.contarNotas(transacao.getNotas(), NotasEnum.CEM));
		assertEquals(0, caixaFacade.contarNotas(transacao.getNotas(), NotasEnum.CINQUENTA));
		assertEquals(28, caixaFacade.contarNotas(transacao.getNotas(), NotasEnum.VINTE));
		assertEquals(0, caixaFacade.contarNotas(transacao.getNotas(), NotasEnum.DEZ));		
	}
}
