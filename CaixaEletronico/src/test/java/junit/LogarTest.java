package junit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.zenvia.facade.ContaFacadeImp;
import br.com.zenvia.model.Conta;
import br.com.zenvia.model.Fisica;

public class LogarTest {
	
	ContaFacadeImp contaFacade = new ContaFacadeImp();
	
	@Test
	public void testLogar() {
		Fisica pessoa = new Fisica("Leandro", "Rua das Perdizes", "leocpnet@gmail.com", true, 99999999999L);
		Conta conta = new Conta(pessoa, 123456789L, 1234);
		//Senha incorreta
		contaFacade.logar(conta, 999);
		assertFalse(conta.isLogado());
		//Senha correta
		contaFacade.logar(conta, 1234);
		assertTrue(conta.isLogado());		
	}

}
